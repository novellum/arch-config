# arch-config

Simple and sweet install script for arch linux

# Instructions
pacman -Sy git
git clone https://gitlab.com/novellum/arch-config
cd arch-config
sh install.sh
