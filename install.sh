# Sort mirrors
print "Sort mirrors"
pacman -Sy --needed --noconfirm reflector
cp -v -f /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
reflector --verbose --protocol https --latest 20 --sort rate --save /etc/pacman.d/mirrorlist
pacman -Syu

pacman -Sy wget

wget archfi.sf.net/archfi
sh archfi

# Chroot mirrors
cp -v -f /mnt/etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist.backup
cp -v -f /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist

# Prepare locales and keymap
print "Prepare locales and keymap"
echo "KEYMAP=dvorak" > /mnt/etc/vconsole.conf
sed -i 's/#\(en_US.UTF-8\)/\1/' /mnt/etc/locale.gen
echo 'LANG="en_US.UTF-8"' > /mnt/etc/locale.conf

# Install
print "Install Arch Linux"
pacstrap /mnt base base-devel sudo wpa_supplicant reflector zsh git ninja wget

# Configure sudo
cat > /mnt/etc/sudoers <<"EOF"
Defaults:%wheel targetpw
%wheel ALL=(ALL) ALL
Defaults targetpw
EOF

# Set hostname
echo "Please enter hostname :"
read hostname
echo $hostname > /mnt/etc/hostname

# 32bit support
cat > /mnt/etc/pacman.conf <<"EOF"
[multilab]
Include = /etc/pacmand.d/mirrorlist
EOF

arch-chroot /mnt /bin/bash -xe <<"EOF"
  echo "Please enter name of user :"
  read $user
  useradd -m -g users -G wheel -s /bin/zsh $user
  passwd $user
  su $user

  git clone https://aur.archlinux.org/yay.git
  cd yay
  makepkg -si

  yay -Sy yadm
  yadm clone https://gitlab.com/novellum/dotfiles-yadm
EOF

