#!/usr/bin/env bash

set -e

# Check if /mnt is a mountpoint
mountpoint / || exit 0

print () {
    echo -e "\n\033[1m> $1\033[0m\n"
}

# Install
print "Install Arch Linux"
pacstrap /mnt base base-devel linux linux-firmware linux-headers intel-ucode btrfs-progs efibootmgr vim git snapper wpa_supplicant reflector zsh

# Set hostname
echo "Please enter hostname :"
read hostname
echo $hostname > /mnt/etc/hostname

# Configure /etc/hosts
print "Configure hosts file"
cat > /mnt/etc/hosts <<EOF
#<ip-address>	<hostname.domain.org>	<hostname>
127.0.0.1	    localhost   	        $hostname
::1   		    localhost              	$hostname
EOF

# Prepare locales and keymap
print "Prepare locales and keymap"
echo "KEYMAP=dvorak" > /mnt/etc/vconsole.conf
sed -i 's/#\(en_US.UTF-8\)/\1/' /mnt/etc/locale.gen
echo 'LANG="en_US.UTF-8"' > /mnt/etc/locale.conf

# Prepare initramfs
print "Prepare initramfs"
cat > /mnt/etc/mkinitcpio.conf <<"EOF"
MODULES=(i915 intel_agp)
BINARIES=(/usr/bin/btrfs)
FILES=()
HOOKS=(base systemd autodetect modconf block keyboard sd-vconsole sd-encrypt fsck filesystems)
COMPRESSION="lz4"
EOF

# Chroot and configure
print "Chroot and configure system"

arch-chroot /mnt /bin/bash -xe <<"EOF"
  # Sync clock
  hwclock --systohc
  # Set date
  timedatectl set-ntp true
  timedatectl set-timezone America/New_York
  # Generate locale
  locale-gen
  source /etc/locale.conf
  # Generate Initramfs
  mkinitcpio -p linux
  # Prepare grub2
  #sed -i 's/#\(GRUB_ENABLE_CRYPTODISK=y\)/\1/' /etc/default/grub
  # Create user
  useradd -m -g users -G wheel -s /bin/zsh user
EOF

# Set root passwd
print "Set root password"
arch-chroot /mnt /bin/passwd

# Set user passwd
print "Set user password"
arch-chroot /mnt /bin/passwd user

# Configure sudo
cat > /mnt/etc/sudoers <<"EOF"
Defaults:%wheel targetpw
%wheel ALL=(ALL) ALL
Defaults targetpw
EOF

# 32bit support
cat > /mnt/etc/pacman.conf <<"EOF"
[multilab]
Include = /etc/pacmand.d/mirrorlist
EOF

# Configure network
systemctl enable systemd-networkd --root=/mnt
systemctl disable systemd-networkd-wait-online --root=/mnt

# Configure DNS
rm /mnt/etc/resolv.conf
arch-chroot /mnt ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
systemctl enable systemd-resolved --root=/mnt

# Configure TRIM
systemctl enable fstrim.timer --root=/mnt

# Umount all parts
umount -R /mnt

# Finish
echo -e "\e[32mAll OK"
